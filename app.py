import hashlib
import json
import os
import csv
import random
import unicodedata
from sortedcontainers import SortedDict
from dateutil.relativedelta import relativedelta
import datetime

from flask import Flask, request, render_template, session, jsonify
from flask_session import Session

from database_connection import initialize_db

app = Flask(__name__, static_url_path="/static")

app.config['SESSION_TYPE'] = 'filesystem'

app.secret_key = 'fxnetting'

app.config.from_object(__name__)
Session(app)
DIR_PATH = os.path.dirname(os.path.realpath(__file__))


@app.route('/')
def index():
    return render_template("login.html")


@app.route('/show_add_trade')
def show_add_trade():
    return render_template("add_trade.html")


@app.route('/import_trade')
def import_trade():
    return render_template("import_trade.html")


@app.route('/import_trade_from_csv', methods=['POST'])
def import_trade_from_csv():
    f = open('data.csv', 'r')
    reader = csv.reader(f)
    for row in reader:
        query = "INSERT into trades (client_code, client_name,  ccy_pair, base_notional, base_direction, " \
                "price , trade_type, quote_ccy_qty, trade_date, " \
                "value_date) VALUES (%s, %s, %s, %s,%s, %s, %s, %s, %s, %s);"

        my_cursor.execute(query, row)

        # close the connection to the database.
        my_db.commit()

    return "Trade has been imported into the database"


@app.route('/add_trade', methods=['POST'])
def add_trade():
    # result = request.form
    # return str(result)
    client_code = request.form['client_code']
    currenc_pair = request.form.get('currency_pair')
    base_direction = request.form['base_direct']
    base_direct = ''
    if base_direction == u'1':
        base_direct = 'B'
    if base_direction == u'2':
        base_direct = 'S'

    base_notional = request.form['base_notional']
    trading_type = request.form['trade_type']
    trade_type = ""
    if trading_type == u'1':
        trade_type = "SPOT"
    if trading_type == u'2':
        trade_type = "TOM"

    trade_date = request.form['trade_date']
    price = request.form['price']

    currency_map = {"0": "", "1": "GBP/USD", "2": "GBP/EUR", "3": "EUR/USD"}

    client_code = client_code.encode('ascii', 'ignore')
    currency_pair = currency_map[currenc_pair]
    base_notional = base_notional.encode('ascii', 'ignore')
    trade_date = trade_date.encode('ascii', 'ignore')
    price = price.encode('ascii', 'ignore')
    quote_ccy_qty = (int)(base_notional) * (float)(price)
    print(type(quote_ccy_qty))

    trade_date = datetime.datetime.strptime(trade_date, "%Y-%m-%d")
    d = 0
    if trade_type == "SPOT":
        d = 2
    elif trade_type == "TOM":
        d = 1

    addDays = relativedelta(days=d)

    value_date = trade_date + addDays
    value_date = value_date.date()

    search_client_name_query = "SELECT client_name FROM client WHERE client_code = %s;"
    my_cursor.execute(search_client_name_query, [client_code])
    row = my_cursor.fetchall()
    client_name = str(row[0][0])
    # client_name = client_name.encode('ascii', 'ignore')
    query = "INSERT into trades (client_code, client_name, ccy_pair, base_notional, base_direction, " \
            "price, trade_type, quote_ccy_qty, trade_date, value_date) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s);"
    my_cursor.execute(query, [client_code, client_name, currency_pair,
                              (int)(base_notional), base_direct, float(price), trade_type, quote_ccy_qty,
                              trade_date, value_date])

    return "Trade added successfully"


@app.route('/update_net_value', methods=['GET'])
def net_by_currency():
    currency_data = get_value_from_db()
    currency_data = SortedDict(currency_data)
    return jsonify(currency_data)


@app.route('/handle_login', methods=['POST'])
def handle_login():
    # receive post response
    username = request.form['userid']
    pwd = request.form['password']

    # calculate hash of password
    pwd = hashlib.md5(str(pwd).encode('utf-8')).hexdigest()

    # check credentials
    my_cursor.execute("SELECT COUNT(1) FROM employee WHERE username = %s;", [username])  # CHECKS IF USERNAME EXSIST
    if my_cursor.fetchone()[0]:
        my_cursor.execute("SELECT password FROM employee WHERE username = %s;", [username])
        password = (str(my_cursor.fetchone()[0]))

        if pwd == password:
            print("Login Successful")

            session["uname"] = str(username)
            currency_data = get_value_from_db()
            currency_data = SortedDict(currency_data)
            return show_trade_book()

    else:
        # prompt user doesn't exist
        pass


def get_value_from_db():
    mul_query = "SELECT ccy_pair, SUM(CASE " \
                "base_direction " \
                "WHEN 'B' THEN quote_ccy_qty " \
                "WHEN 'S' THEN -quote_ccy_qty END) " \
                "FROM trades GROUP BY ccy_pair order by ccy_pair asc;"
    query = "SELECT ccy_pair, SUM(CASE " \
            "base_direction " \
            "WHEN 'B' THEN base_notional " \
            "WHEN 'S' THEN -base_notional END) " \
            "FROM trades GROUP BY ccy_pair order by ccy_pair asc;"
    my_cursor.execute(mul_query)
    my_cursor.execute(query)
    rows = my_cursor.fetchall()
    currency_data = {}
    for row in rows:
        currency_data[row[0]] = row[1]
    return currency_data


@app.route('/test')
def test():
    # currency_data = {}
    # return render_template("index.html", currency_data=currency_data)
    get_value_from_db()


def execute_my_query(query, param=None):
    my_cursor.execute(query, param)
    trades_data = my_cursor.fetchall()
    trades = []
    for row in trades_data:
        trade = {}
        trade['trade_id'] = row[0]
        trade['client_code'] = row[1]
        trade['client_name'] = row[2]
        trade['ccy_pair'] = row[3]
        trade['base_notional'] = row[4]
        trade['base_dir'] = row[5]
        trade['price'] = row[6]
        trade['trade_type'] = row[7]
        trade['quote_ccy_qty'] = row[8]
        trade['trade_date'] = row[9]
        trade['value_date'] = row[10]
        trades.append(trade)

    return trades


@app.route('/trade_book', methods=['GET'])
def show_trade_book():
    trades = execute_my_query("select * from trades;")
    currency_data = {}
    return render_template("trade_book.html", trades=trades, currency_data=currency_data)


@app.route('/filter_book', methods=['POST', 'GET'])
def filter_book():
    result = request.form

    currency_data = {}
    # print result
    # cur_pair is not present if not touched
    # client_name is empty, if not empty

    cp = False
    cn = True

    empty = u''
    cur_pair = u''

    if 'cur_pair' in result:
        cp = True
        cur_pair = result['cur_pair']
    if result['client_name'] == empty:
        cn = False

    if cn and cp:
        currency_map = {"0": "", "1": "GBP/USD", "2": "GBP/EUR", "3": "EUR/USD"}
        currency = currency_map[cur_pair]
        client_name = result['client_name']
        client_name = client_name.encode('ascii', 'ignore')
        param = (currency, client_name)
        query = "select * from trades where ccy_pair = %s and client_name = %s;"
        trades = execute_my_query(query, param)
        return render_template("trade_book.html", trades=trades)
    elif cn:
        client_name = result['client_name']
        client_name = client_name.encode('ascii', 'ignore')
        param = (client_name,)
        query = "select * from trades where client_name = %s;"
        trades = execute_my_query(query, param)

        mul_query = "SELECT ccy_pair, SUM(CASE base_direction WHEN 'B' THEN -quote_ccy_qty WHEN 'S' THEN quote_ccy_qty END) FROM trades WHERE client_name=%s GROUP BY ccy_pair order by ccy_pair asc;"
        my_cursor.execute(mul_query, [client_name])
        rows = my_cursor.fetchall()
        query = "SELECT ccy_pair, SUM(CASE base_direction WHEN 'B' THEN base_notional WHEN 'S' THEN -base_notional END) FROM trades WHERE client_name=%s GROUP BY ccy_pair order by ccy_pair asc;"
        my_cursor.execute(query, [client_name])
        rows1 = my_cursor.fetchall()

        print(rows)
        print(rows1)

        # for r in rows1:
        #     cur1 = r[0].split('/')
        #     print(cur1)
        #     #print(type(r[1]))
        #     currency_data[cur1[0]] = [str(abs(r[1])), 'Payable' if r[1] > 0 else 'Receivable']
        #
        #
        # for r in rows:
        #     cur1 =  r[0].split('/')
        #     print(cur1)
        #     if cur1[0] in currency_data:
        #         currency_data[cur1[0]] =
        #
        # currency_data[cur1[1]] = [str(abs(r[1])), 'Payable' if r[1] > 0 else 'Receivable']
        # cur1 = currency.split('/')
        # print(cur1)
        # print(type(rows1[0][1]))
        # currency_data[cur1[0]] = [str(abs(rows1[0][1])), 'Payable' if rows1[0][1] > 0 else 'Receivable']
        # currency_data[cur1[1]] = [str(abs(rows[0][1])), 'Payable' if rows[0][1] > 0 else 'Receivable']

        return render_template("trade_book.html", trades=trades, currency_data=currency_data)
    elif cp:
        currency_map = {"0": "", "1": "GBP/USD", "2": "GBP/EUR", "3": "EUR/USD"}
        currency = currency_map[cur_pair]
        param = (currency,)
        query = "select * from trades where ccy_pair = %s;"
        trades = execute_my_query(query, param)
        print(currency)
        mul_query = "SELECT ccy_pair, SUM(CASE base_direction WHEN 'B' THEN -quote_ccy_qty WHEN 'S' THEN quote_ccy_qty END) FROM trades WHERE ccy_pair=%s GROUP BY ccy_pair order by ccy_pair asc;"
        my_cursor.execute(mul_query, [currency])
        rows = my_cursor.fetchall()
        query = "SELECT ccy_pair, SUM(CASE base_direction WHEN 'B' THEN base_notional WHEN 'S' THEN -base_notional END) FROM trades WHERE ccy_pair=%s GROUP BY ccy_pair order by ccy_pair asc;"
        my_cursor.execute(query, [currency])
        rows1 = my_cursor.fetchall()

        print(rows[0][1])
        print(rows1)

        cur1 = currency.split('/')
        print(cur1)
        print(type(rows1[0][1]))
        currency_data[cur1[0]] = [str(abs(rows1[0][1])), 'Payable' if rows1[0][1] > 0 else 'Receivable']
        currency_data[cur1[1]] = [str(abs(rows[0][1])), 'Payable' if rows[0][1] > 0 else 'Receivable']

        print(currency_data)

        return render_template("trade_book.html", trades=trades, currency_data=currency_data)
    else:
        query = "select * from trades;"
        trades = execute_my_query(query)
        return render_template("trade_book.html", trades=trades)


if __name__ == '__main__':
    my_cursor, my_db = initialize_db()
    port = int(os.environ.get("PORT", 5000))
    app.run(debug=True, host='localhost', port=port)
    my_cursor.close()
    my_db.close()
